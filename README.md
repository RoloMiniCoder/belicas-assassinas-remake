# Belica assassina !

This is a complete from scrath refactoring from the game [Belica][belica-gitlab], from [Luisa][luisa-gitlab], [Pedro Vieira][vieira-gitlab], [Ricardo][ricardo-gitlab] and [João][joao-gitlab].

Made as an instructional example to showcase some different strategies, thoughts and approaches at the same problem/behaviour.

[belica-gitlab]:<https://github.com/eddieatjollyroger/BelicaAssassina>
[luisa-gitlab]: <https://gitlab.com/luisasopas>
[vieira-gitlab]: <https://gitlab.com/PedroVieira1>
[ricardo-gitlab]:<https://gitlab.com/RicardoBett>
[joao-gitlab]:<https://gitlab.com/gglowss>
